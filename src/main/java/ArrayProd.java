public class ArrayProd {
    int [] arrProd;
    public ArrayProd(int[] arrProd) {
        this.arrProd = arrProd;
    }
    public int prod (){
        int prod = 1;
        for (int each : arrProd){
            prod *=each;
        }
        return prod;
    }

    public static int prod (int [] arrProd) throws NullPointerException {
        if (arrProd == null) throw new NullPointerException();
        int prod = 1;
        for (int each : arrProd){
            prod *=each;
        }
        return prod;
    }


    public static void main(String[] args) {

    }
}
