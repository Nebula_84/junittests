import java.util.ArrayList;
import java.util.List;

public class Student {
    String firstName;
    String lastName;
    //Group groupID;
    ArrayList<Exam> exams;

    public Student(String firstName, String lastName, ArrayList<Exam> exams) {
        this.firstName = firstName;
        this.lastName = lastName;

        this.exams = exams;
    }

    public int findMaxMark(String examName) {
        int max = 0;
        for (Exam ex : exams) {
            if (ex.getExamName().equals(examName) & (max < ex.getMark())) {
                max = ex.getMark();
            }
        }
        return max;
    }

    public void setExamMark(String examName, int mark) {
        exams.add(new Exam(examName, mark));
    }

    public void removeExamMark(String examName) {
        for (Exam ex : exams) {
            if (ex.getExamName().equals(examName)) ex.removeMark();
        }
    }

    public void checkIfExamNamePresent(String examName) throws NoSuchExamNameException {
        boolean ifNameEquals = false;
            for (Exam ex : exams) {
                if (ex.getExamName().equals(examName)) {
                    ifNameEquals = true;
                    System.out.println("there is an exam with same name");
                    break;
                }
            }
            if (!ifNameEquals) throw new NoSuchExamNameException();


    }


    public int getNumOfExamsWithSuchMark(int mark) {
        int counter = 0;
        for (Exam ex : exams) {
            if (ex.getMark() == mark) counter++;
        }
        return counter;
    }

    public double getAverageMarkOfTheSemester(int semester) {
        double sum = 0;
        int counter = 0;
        for (Exam ex : exams) {
            if (ex.getSemester() == semester){
                sum +=ex.getMark();
                counter++;
            }
        }
        return Math.round((sum / counter)*100.)/100.;
    }

    @Override
    public String toString() {
        return "Student{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", exams=" + exams +
                '}';
    }

    public static void main(String[] args) throws NoSuchExamNameException {

        ArrayList<Exam> exams = new ArrayList<Exam>();
        exams.add(new Exam("Maths", 5));
        exams.add(new Exam("Chemistry", 2));
        exams.add(new Exam("Maths", 3));
        exams.add(new Exam("Physics", 3));
        exams.add(new Exam("Maths", 12));
        Student st = new Student("Kateryna", "Latsko", exams);
        System.out.println(st.findMaxMark("Physics"));
        st.setExamMark("Lit", 10);
        System.out.println(st);
        System.out.println(exams.get(exams.size() - 1).getMark());
        //st.checkIfExamNamePresent("Medicine");
        System.out.println(st.getNumOfExamsWithSuchMark(1));
    }


}

