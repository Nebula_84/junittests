public class Exam {
    String examName;
    int mark;
    int[] arrMarks;
    int year;
    int semester;

    public Exam(String examName) {
        this.examName = examName;
    }
    public Exam(String examName, int mark) {
        this.examName = examName;
        this.mark = mark;
    }

    public Exam(String examName, int mark, int semester) {
        this.examName = examName;
        this.mark = mark;
        this.semester = semester;
    }

    public String getExamName() {
        return examName;
    }

    public int getMark() {
        return mark;
    }

    public int getSemester() {
        return semester;
    }

    public int setMark(int mark) {
        this.mark = mark;
        return mark;
    }

    public int removeMark() {
        mark = 0;
        return mark;

    }

    @Override
    public String toString() {
        return "Exam{" +
                "examName='" + examName + '\'' +
                ", mark=" + mark +
                ", year='" + year + '\'' +
                '}';
    }
}
