import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ArraySumTest {


    //Test for instance method sum ()
    private ArraySum as;

    @Before
    public void setUp() {
        int[] arrSum = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        as = new ArraySum(arrSum);
    }

    @Test
    public void sum() {
        int expected = 45;
        int actual = as.sum();
        Assert.assertEquals(expected, actual);
    }

    @After
    public void tearDown() {
        as = null;
    }


    //Test for static method sum()
    @Test
    public void sumS() {
        int[] arrSum = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        int expected = 45;
        int actual = ArraySum.sum(arrSum);
        Assert.assertEquals(expected, actual);
    }


    //Test of NullPointerException for static method sum ()
    @Test (expected = NullPointerException.class)
    public void sumEx() throws NullPointerException {
        ArraySum.sum(null);
    }
}