import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ExamTest {
    Exam ex;

    //Create an object of Exam class
    @Before
    public void setUp() {
        ex = new Exam("Maths");
    }


    //Set mark for a single Exam
    @Test
    public void setMarkTest() {
        int expected = 5;
        int actual = ex.setMark(5);
        System.out.println(ex);
        Assert.assertEquals(expected, actual);
    }


    //Remove mark of the Exam
    @Test
    public void removeMarkTest() {
        int expected = 0;
        int actual = ex.removeMark();
        System.out.println(ex);
        Assert.assertEquals(expected, actual);
    }

    @After
    public void tearDown() {

    }
}