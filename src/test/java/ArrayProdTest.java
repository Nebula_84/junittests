import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ArrayProdTest {
    private ArrayProd ap;

    @Before
    public void setUp() {
        int[] arrP = {1, 2, 3};
        ap = new ArrayProd(arrP);
    }
    @Test
    public void prod() {
        int expected = 6;
        int actual = ap.prod();
        Assert.assertEquals(expected, actual);

    }

    @Test
    public void prodSt() throws Exception {
        int[] arrP = {0,1,2,3};
        int expected = 0;
        int actual = ArrayProd.prod(arrP);
        Assert.assertEquals(expected, actual);
    }

}