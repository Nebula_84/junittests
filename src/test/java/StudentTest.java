import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

public class StudentTest {

    Student st;
    ArrayList<Exam> exams;


    //Calculate max mark of all exams
    @Before
    public void setUpMaxMarkTest() {
        exams = new ArrayList<Exam>();
        exams.add(new Exam("Maths", 5,1));
        exams.add(new Exam("Chemistry", 2,2));
        exams.add(new Exam("Maths", 3,2));
        exams.add(new Exam("Physics", 4,1));
        exams.add(new Exam("Maths", 12,1));
        exams.add(new Exam("Biology", 3,1));
        exams.add(new Exam("Maths", 3,2));
        st = new Student("Kateryna", "Latsko", exams);
    }

    @Test
    public void findMaxMarkTest() {
        int expected = 12;
        int actual = st.findMaxMark("Maths");
        Assert.assertEquals(expected, actual);
    }


    //Set and remove mark for an exam
   /* @Before
    public void setUpSetExamMark() {

        st.setExamMark("Lit", 10);
    }

    @Test
    public void setExamMarkTest() {
        int expected = 10;
        //st.setExamMark("Lit", 10);
        int actual = exams.get(exams.size()-1).getMark();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void removeExamMarkTest() {
        int expected = 0;
        st.removeExamMark("Lit");
        int actual = exams.get(exams.size()-1).getMark();
        Assert.assertEquals(expected, actual);
    }
*/
    @After
    public void tearDown() {

    }


//!!! try-catch? Throw NuSuchExamNameException if an exam name doesn't exist in the list
    @Test(expected = NoSuchExamNameException.class)
    public void checkIfExamNamePresentTest() throws NoSuchExamNameException {
        st.checkIfExamNamePresent("Medicine");
    }


//Return total number of exams with such mark
    @Test
    public void getNumOfExamsWithSuchMarkTest(){
        int expected  = 3;
        int actual = st.getNumOfExamsWithSuchMark(3);
        Assert.assertEquals(expected,actual);
    }
//Return average mark for specified semester
    @Test
    public void getAverageMarkOfTheSemesterTest(){
        double expected = 2.6;
        double actual = st.getAverageMarkOfTheSemester(2);
        Assert.assertEquals(expected, actual, 0.1);
    }
}